﻿namespace FrmTxtXml
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtServidor = new System.Windows.Forms.TextBox();
            this.txtBanco = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtConteudo = new System.Windows.Forms.TextBox();
            this.bntGravarTXT = new System.Windows.Forms.Button();
            this.bntGravarXML = new System.Windows.Forms.Button();
            this.bntLerTXT = new System.Windows.Forms.Button();
            this.bntLerXML = new System.Windows.Forms.Button();
            this.gpBoxTXT = new System.Windows.Forms.GroupBox();
            this.gpBoxXML = new System.Windows.Forms.GroupBox();
            this.gpBoxTXT.SuspendLayout();
            this.gpBoxXML.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Servidor:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Banco:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 98);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Usuário:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Senha:";
            // 
            // txtServidor
            // 
            this.txtServidor.Location = new System.Drawing.Point(66, 39);
            this.txtServidor.Name = "txtServidor";
            this.txtServidor.Size = new System.Drawing.Size(163, 20);
            this.txtServidor.TabIndex = 4;
            // 
            // txtBanco
            // 
            this.txtBanco.Location = new System.Drawing.Point(66, 65);
            this.txtBanco.Name = "txtBanco";
            this.txtBanco.Size = new System.Drawing.Size(163, 20);
            this.txtBanco.TabIndex = 5;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(66, 91);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(163, 20);
            this.txtUsuario.TabIndex = 6;
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(66, 117);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '*';
            this.txtSenha.Size = new System.Drawing.Size(163, 20);
            this.txtSenha.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Conteúdo:";
            // 
            // txtConteudo
            // 
            this.txtConteudo.Location = new System.Drawing.Point(11, 206);
            this.txtConteudo.Multiline = true;
            this.txtConteudo.Name = "txtConteudo";
            this.txtConteudo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtConteudo.Size = new System.Drawing.Size(253, 236);
            this.txtConteudo.TabIndex = 9;
            // 
            // bntGravarTXT
            // 
            this.bntGravarTXT.Location = new System.Drawing.Point(108, 37);
            this.bntGravarTXT.Name = "bntGravarTXT";
            this.bntGravarTXT.Size = new System.Drawing.Size(75, 23);
            this.bntGravarTXT.TabIndex = 10;
            this.bntGravarTXT.Text = "GravarTXT";
            this.bntGravarTXT.UseVisualStyleBackColor = true;
            this.bntGravarTXT.Click += new System.EventHandler(this.bntGravarTXT_Click);
            // 
            // bntGravarXML
            // 
            this.bntGravarXML.Location = new System.Drawing.Point(108, 59);
            this.bntGravarXML.Name = "bntGravarXML";
            this.bntGravarXML.Size = new System.Drawing.Size(75, 23);
            this.bntGravarXML.TabIndex = 11;
            this.bntGravarXML.Text = "GravarXML";
            this.bntGravarXML.UseVisualStyleBackColor = true;
            this.bntGravarXML.Click += new System.EventHandler(this.bntGravarXML_Click);
            // 
            // bntLerTXT
            // 
            this.bntLerTXT.Location = new System.Drawing.Point(6, 37);
            this.bntLerTXT.Name = "bntLerTXT";
            this.bntLerTXT.Size = new System.Drawing.Size(75, 23);
            this.bntLerTXT.TabIndex = 12;
            this.bntLerTXT.Text = "LerTxt";
            this.bntLerTXT.UseVisualStyleBackColor = true;
            this.bntLerTXT.Click += new System.EventHandler(this.bntLerTXT_Click);
            // 
            // bntLerXML
            // 
            this.bntLerXML.Location = new System.Drawing.Point(6, 59);
            this.bntLerXML.Name = "bntLerXML";
            this.bntLerXML.Size = new System.Drawing.Size(75, 23);
            this.bntLerXML.TabIndex = 13;
            this.bntLerXML.Text = "LerXML";
            this.bntLerXML.UseVisualStyleBackColor = true;
            this.bntLerXML.Click += new System.EventHandler(this.bntLerXML_Click);
            // 
            // gpBoxTXT
            // 
            this.gpBoxTXT.Controls.Add(this.bntGravarTXT);
            this.gpBoxTXT.Controls.Add(this.bntLerTXT);
            this.gpBoxTXT.Location = new System.Drawing.Point(278, 217);
            this.gpBoxTXT.Name = "gpBoxTXT";
            this.gpBoxTXT.Size = new System.Drawing.Size(200, 100);
            this.gpBoxTXT.TabIndex = 14;
            this.gpBoxTXT.TabStop = false;
            this.gpBoxTXT.Text = "Arquivo TXT";
            // 
            // gpBoxXML
            // 
            this.gpBoxXML.Controls.Add(this.bntLerXML);
            this.gpBoxXML.Controls.Add(this.bntGravarXML);
            this.gpBoxXML.Location = new System.Drawing.Point(278, 39);
            this.gpBoxXML.Name = "gpBoxXML";
            this.gpBoxXML.Size = new System.Drawing.Size(200, 109);
            this.gpBoxXML.TabIndex = 15;
            this.gpBoxXML.TabStop = false;
            this.gpBoxXML.Text = "Arquivo XML";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 465);
            this.Controls.Add(this.gpBoxXML);
            this.Controls.Add(this.gpBoxTXT);
            this.Controls.Add(this.txtConteudo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.txtBanco);
            this.Controls.Add(this.txtServidor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Gerador De Arquivos Tabajara";
            this.gpBoxTXT.ResumeLayout(false);
            this.gpBoxXML.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtServidor;
        private System.Windows.Forms.TextBox txtBanco;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtConteudo;
        private System.Windows.Forms.Button bntGravarTXT;
        private System.Windows.Forms.Button bntGravarXML;
        private System.Windows.Forms.Button bntLerTXT;
        private System.Windows.Forms.Button bntLerXML;
        private System.Windows.Forms.GroupBox gpBoxTXT;
        private System.Windows.Forms.GroupBox gpBoxXML;
    }
}

