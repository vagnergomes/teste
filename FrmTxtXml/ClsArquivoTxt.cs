﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FrmTxtXml
{
    class ClsArquivoTxt
    {
        public void Fnc_GravarTxt(string servidor, string banco, string usuario, string senha)
        {
            StreamWriter stwArquivoTxt = new StreamWriter("d:\\ArquivoTexto.txt");
            
            stwArquivoTxt.WriteLine("Servidor: " + servidor.Trim());
            stwArquivoTxt.WriteLine("Banco: " + banco.Trim());
            stwArquivoTxt.WriteLine("Usuario: " + usuario.Trim());
            stwArquivoTxt.WriteLine("senha " + senha.Trim());
            //stwArquivoTxt.WriteLine("Banco: " + banco.Trim());

            stwArquivoTxt.Close();

        }

        public string Fnc_LerTxt()
        {
            string strTextoFinal = "";

            try
            {
                StreamReader strArquivotxt = new StreamReader("d:\\ArquivoTexto.txt");
                string linha = "";
                while ((linha = strArquivotxt.ReadLine()) != null){
                    strTextoFinal += linha + "\r\n";
                }
                strArquivotxt.Close();
            }
            catch(Exception ex)
            {
                strTextoFinal = ex.Message;
            }

            return strTextoFinal;
        }


        
    }
}
