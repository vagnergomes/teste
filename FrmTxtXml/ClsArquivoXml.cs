﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace FrmTxtXml
{
    class ClsArquivoXml
    {
        public void Fnc_GravarXML(string servidor, string banco, string usuario, string senha)
        {


            XmlTextWriter ArquivoXML = new XmlTextWriter("d:\\ArquivoXML.xml", Encoding.UTF8);
            ArquivoXML.WriteStartElement("Configuração");
            ArquivoXML.WriteElementString("Servidor", servidor.Trim());
            ArquivoXML.WriteElementString("Banco", banco.Trim());
            ArquivoXML.WriteElementString("Usuario", usuario.Trim());
            ArquivoXML.WriteElementString("Senha", senha.Trim());

            ArquivoXML.Close();

        }
        public string Fnc_lerXML()
        {
            string strTextoFinal = "";

            try
            {
                XElement XML = XElement.Load("d:\\ArquivoXML.xml");
                foreach(XElement x in XML.Elements())
                {
                    strTextoFinal += x.Name + ":" + x.Value + "\r\n";
                }
                XML = null;
            }
            catch(Exception ex)
            {
                strTextoFinal = ex.Message;
            }

            return strTextoFinal;
        }
    }
}
