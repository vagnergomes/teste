﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FrmTxtXml
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void bntGravarTXT_Click(object sender, EventArgs e)
        {
            ClsArquivoTxt arquivoTxt = new ClsArquivoTxt();
            arquivoTxt.Fnc_GravarTxt(txtServidor.Text, txtBanco.Text, txtUsuario.Text, txtSenha.Text);
            txtServidor.Text = "";
            txtBanco.Text = "";
            txtUsuario.Text = "";
            txtSenha.Text = "";
        }

        private void bntLerTXT_Click(object sender, EventArgs e)
        {
            if (txtConteudo.Text != null)
            {
                txtConteudo.Text = null;
            }
            else
            {
                ClsArquivoTxt arquivoTxt = new ClsArquivoTxt();
                txtConteudo.Text = arquivoTxt.Fnc_LerTxt();
            }
        }

        private void bntGravarXML_Click(object sender, EventArgs e)
        {
            ClsArquivoXml arquivoXml = new ClsArquivoXml();
            arquivoXml.Fnc_GravarXML(txtServidor.Text, txtBanco.Text, txtUsuario.Text, txtSenha.Text);
            txtServidor.Text = "";
            txtBanco.Text = "";
            txtUsuario.Text = "";
            txtSenha.Text = "";
        }

        private void bntLerXML_Click(object sender, EventArgs e)
        {
            if (txtConteudo.Text != null)
            {
                txtConteudo.Text = null;
            }
            else
            {
                ClsArquivoXml arquivoXML = new ClsArquivoXml();
                txtConteudo.Text = arquivoXML.Fnc_lerXML();
            }
        }
    }
}
